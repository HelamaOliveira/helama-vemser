public class Dwarf extends Personagem {
    private boolean equipado;
    
    {
        this.equipado = false;
        this.qtdDano = 10.0;
    }
    
    public Dwarf( String nome ) {
        super(nome);
        this.vida = 110.0;
        this.inventario.adicionar(new Item(1, "Escudo"));
    }
    
    protected double calcularDano() {
        return this.equipado ? 5.0 : this.qtdDano;
    }
    
    public void equiparEscudo() {
        this.equipado = true;
    }
    
    /*public void mudarEscudo() {
        this.equipado = !this.equipado;
    }*/
    
    
    
    
    
}