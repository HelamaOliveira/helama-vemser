public class Elfo extends Personagem {
    private int indiceFlecha;
    
    {
        this.indiceFlecha = 1;
    }
    
    public Elfo( String nome ) {
        super(nome);
        this.vida = 100.0;
        this.inventario.adicionar(new Item(1, "Arco"));
        this.inventario.adicionar(new Item(2, "Flecha"));
    }
    
    public Item getFlecha() {
        return this.inventario.obter(indiceFlecha);
    }
    
    public int getQtdFlechas() {
        return this.getFlecha().getQuantidade();
    }
    
    private boolean podeAtirarFlecha() {
        return this.getQtdFlechas() > 0;
    }
    
    public void atirarFlecha(Dwarf dwarf) {
        int qtdAtual = this.getQtdFlechas();
        if( podeAtirarFlecha() ) {
            this.getFlecha().setQuantidade(qtdAtual - 1);
            //this.experiencia = experiencia + 1;
            aumentarXP();
            this.sofrerDano();
            dwarf.sofrerDano();
        }
    }
    
}
